//create the map
const map = L.map('mapid').setView([-22.905906, -47.063541], 13);

// create and add tileLayer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

//create icon
const icon = L.icon({
    iconUrl: "/images/map-marker.svg",
    iconSize: [58, 68],
    iconAnchor: [29, 68]
});

let marker

// // create and add marker
map.on('click', (event) => {
    const latitude = event.latlng.lat;
    const longitude = event.latlng.lng;

    document.querySelector('[name=latitude]').value = latitude;
    document.querySelector('[name=longitude]').value = longitude;


    // remove icon
    marker && map.removeLayer(marker);

    //add icon layer
    marker= L.marker([latitude, longitude], {icon})
    .addTo(map)
});

// adicionar o campo de fotos

function addPhotoField(){
    //pegar o container de fotos #fotos
    const container = document.querySelector('#fotos');
    //pegar o container para duplicar .new-upload
    const fieldsContainer = document.querySelectorAll('.new-upload');
    //realizar o clone da ultima imagem adicionada
    const newFieldContainer = fieldsContainer[fieldsContainer.length -1].cloneNode(true);
    // verificar se o campo esta vazio
    const input = newFieldContainer.children[0];
    if(input.value === ""){
        return
    }
    //limpar o campo
    input.value = "";
    //adicionar o clone ao container de #fotos
    container.appendChild(newFieldContainer);
}

function deleteLink(event) {
    const span = event.currentTarget

    const fieldsContainer = document.querySelectorAll('.new-upload');

    if(fieldsContainer.length < 2){
        //limpar o campo
        span.parentNode.children[0].value = "";
        return
    }

    //deletar o campo
    span.parentNode.remove();

}

//seleção do sim ou não

function toggleSelect(event){

    // retirar a class .active dos botões
    document.querySelectorAll('.button-select button')
    .forEach((button) =>{
        button.classList.remove('active')
    });

    //colocar a class .active no botão clicado
    const button = event.currentTarget;
    button.classList.add('active');
    // pegar  o botão clicado
    // verificar valor
    // atualizar o input hidden com valo selecionado
    const input = document.querySelector('[name="open_on_weekens"]');
    input.value = button.dataset.value
}


const options = {
    dragging: false,
    touchZoom: false,
    doubleClickZoom: false,
    scrollWheelZoom: false,
    zoomControl: false
}
const latitude = document.querySelector('[data-latitude]').dataset.latitude;
const longitude = document.querySelector('[data-longitude]').dataset.longitude;

//create the map
const map = L.map('mapid', options).setView([latitude,longitude], 15);

// create and add tileLayer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

//create icon
const icon = L.icon({
    iconUrl: "/images/map-marker.svg",
    iconSize: [58, 68],
    iconAnchor: [29, 68],
    popupAnchor: [170, 2]
});

// create and add marker

L.marker([latitude,longitude], {icon})
    .addTo(map)

// Image Gallery

function selectImage(event){
    const button = event.currentTarget;

    //remover todas as classes .active
    const buttons = document.querySelectorAll(".images button");
    buttons.forEach(removeActiveClass) 

    function removeActiveClass(button){
        button.classList.remove("active")
    }

    //selecionar a imagem clicada
    const image = button.children[0];
    const imageContainer = document.querySelector(".detalhes-orfanato img");
    //atualizar o container de image(principal)
    imageContainer.src = image.src
    //adicionar o .active de volta para o button
    button.classList.add("active");
}
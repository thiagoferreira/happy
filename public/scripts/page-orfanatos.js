// //create the map
// const map = L.map('mapid').setView([-22.905906, -47.063541], 13);

// // create and add tileLayer
// L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

// //create icon
// const icon = L.icon({
//     iconUrl: "/images/map-marker.svg",
//     iconSize: [58, 68],
//     iconAnchor: [29, 68],
//     popupAnchor: [170, 2]
// });

// // create popup overlay
// const popup = L.popup({
//     closeButton: false,
//     className: 'map-popup',
//     minWidth: 240,
//     minHeight: 240
// }).setContent('Casa Maria de Nazaré <a href="orfanato?id=1" class="escolha-orfanato"> <img src="/images/arrow-white.svg"> </a>')

// // create and add marker
// L.marker([-22.905906, -47.063541], {icon})
//     .addTo(map)
//     .bindPopup(popup)

//create the map
const map = L.map('mapid').setView([-22.905906, -47.063541], 13);
// create and add tileLayer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

//create icon
const icon = L.icon({
    iconUrl: "/images/map-marker.svg",
    iconSize: [58, 68],
    iconAnchor: [29, 68],
    popupAnchor: [170, 2]
    
});

function addMarker({id, name, latitude, longitude}){
    console.log('hello')

    // create popup overlay
    const popup = L.popup({
        closeButton: false,
        className: 'map-popup',
        minWidth: 240,
        minHeight: 240
    }).setContent(`${name} <a href="orfanato?id=${id}"> <img src="/images/arrow-white.svg"> </a>`)

    // create and add marker
    L.marker([latitude, longitude], {icon})
        .addTo(map)
        .bindPopup(popup)

        console.log('hello')
}

const orfanatosSpan = document.querySelectorAll('.orfanatos span');
orfanatosSpan.forEach(span => {
    
    const orfanato = {
        id: span.dataset.id,
        name: span.dataset.name,
        latitude: span.dataset.latitude,
        longitude: span.dataset.longitude,
    }
    

    console.log(orfanatosSpan)

    addMarker(orfanato);
});

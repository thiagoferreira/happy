async function saveOrfanato(db, orfanato){
    await db.run(`
        INSERT INTO orfanatos (
            latitude,
            longitude,
            nome,
            sobre,
            whatsapp,
            fotos,
            instructions,
            opening_hours,
            open_on_weekens
        ) VALUES (
            "${orfanato.latitude}",
            "${orfanato.longitude}",
            "${orfanato.nome}",
            "${orfanato.sobre}",
            "${orfanato.whatsapp}",
            "${orfanato.fotos}",
            "${orfanato.instructions}",
            "${orfanato.opening_hours}",
            "${orfanato.open_on_weekens}"
        );    
    `);
}


module.exports = saveOrfanato;
const Database = require('sqlite-async');

//Database.open(__dirname + '/database.sqlite').then(execute);

function execute(db) {
    return db.exec(`
        CREATE TABLE IF NOT EXISTS orfanatos (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            latitude TEXT,
            longitude TEXT,
            nome TEXT,
            sobre TEXT,
            whatsapp TEXT,
            fotos TEXT,
            instructions TEXT,
            opening_hours TEXT,
            open_on_weekens TEXT
        );
    `);
}

module.exports = Database.open(__dirname + '/database.sqlite').then(execute);
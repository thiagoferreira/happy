module.exports = [
    {
        id: 1,
        name: "Casa Maria de Nazaré",
        description: "Presta assistência a crianças de 06 a 15 anos que se encontre em situação de risco e/ou vulnerabilidade social.",
        images: [
            "/images/home1.webp"
        ],
        latitude: "-22.905906",
        longitude: "-22.905906",
        instructions: "Venha como se sentir a vontade e traga muito amor e paciência para dar.",
        opening_hours: "Horário de visitas Das 8h até 18h",
        open_on_weekends: "1"
    },
    {
        id: 2,
        name: "Casa Maria",
        description: "Presta assistência a adolescentes de 15 a 20 anos que se encontre em situação de risco e/ou vulnerabilidade social.",
        images: [
            "/images/carrossel2.webp"
        ],
        latitude: "-22.8825434",
        longitude: "-47.0517271",
        instructions: "Venha como se sentir a vontade e traga muito amor e paciência para dar.",
        opening_hours: "Horário de visitas Das 9h até 15h",
        open_on_weekends: "2"
    }
]
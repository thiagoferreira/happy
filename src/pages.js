//const orfanatos = require('./database/fake')
const database = require('./database/db');
const saveOrfanato = require('./database/saveOrfanato')

module.exports = {
    index(request, response){
        return response.render('index');
    },

    async orfanatos(request, response){
     //   return response.render('orfanatos', {orfanatos});
         //colocar orfanatos pelo banco de dados
        try {
            const db = await database;
            const orfanatos = await db.all("SELECT * FROM orfanatos");
            return response.render('orfanatos', {orfanatos})
        } catch (error) {
            console.log("Erro no DB");
            return response.send("Erro no DB");
        }
     },
    async orfanato(request, response){
        const id = request.query.id;
        try {
            const db = await database;
            const results = await db.all(`SELECT * FROM orfanatos WHERE ID = ${id}`);
            const orfanato = results[0];

            orfanato.fotos = orfanato.fotos.split(",");
            orfanato.firstFoto = orfanato.fotos[0];

            if(orfanato.open_on_weekens === "2"){
                orfanato.open_on_weekens = false
            }
            else{
                orfanato.open_on_weekens = true
            }
            return response.render('orfanato', {orfanato: orfanato})
        } catch (error) {
            console.log(error);
            return response.send("Erro no DB");
        }
    },
    criarOrfanato(request, response){
        return response.render('criar-orfanato');
    },
    
    async saveOrfanato(request, response){
        const fields = request.body;
        console.log(fields)

        // if(Object.values(fields).includes('')){
        //     return response.send('Todos os campos devem ser preenchidos');
        // }

        try {
            const db = await database;
            await saveOrfanato(db, {
                latitude: fields.latitude,
                longitude: fields.longitude,
                nome: fields.nome,
                sobre: fields.sobre,
                whatsapp: fields.whatsapp,
                fotos: fields.fotos.toString(),
                instructions: fields.instructions,
                opening_hours: fields.opening_hours,
                open_on_weekens: fields.open_on_weekens,
            });

            //redirecionar para orfanatos
            return response.redirect('/orfanatos');
            
        } catch (error) {
            console.log(error);
            return response.send('Erro no banco de dados');
        }
    }
}
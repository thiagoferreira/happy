const express = require('express');
const path = require('path');
const page = require('./pages');

//iniciando o express
const server = express()
server
// utilizar body do request
.use(express.urlencoded({extended: true}))
// utilizando os arquivos estaticos
.use(express.static('public'))
//configurar template engine
.set('views', path.join(__dirname, 'views'))
.set('view engine', 'hbs')
//rotas da aplicação
.get('/',page.index)
.get('/orfanatos',page.orfanatos)
.get('/orfanato',page.orfanato)
.get('/criar-orfanato',page.criarOrfanato)
.post('/save-orfanato',page.saveOrfanato)
//ligar o servidorz
server.listen(5500);


# Happy

Projeto construido na Next Level Week da Rocketseat.

**Dependências:**

Para executar o projeto é necessário ter node instalado na máquina.
Para instalar as dependências do projeto basta digitar o seguinte comando na pasta raiz do projeto `npm i`

**Executar o projeto:**

Para executar o projeto basta digitar a linha de comando `npm start` na pasta raiz do projeto.
Para acessar a aplicação no brwoser vá para `localhost:5500`